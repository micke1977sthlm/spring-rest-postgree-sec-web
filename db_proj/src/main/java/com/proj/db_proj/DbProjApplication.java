package com.proj.db_proj;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbProjApplication implements CommandLineRunner{
	@Autowired
	PersonRepository pRepository;
	
	@Autowired
	HobbyRepository hRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(DbProjApplication.class, args);
	}
	@Override
	public void run(String... arg0) throws Exception {
		
	}
}
