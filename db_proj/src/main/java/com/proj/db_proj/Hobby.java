package com.proj.db_proj;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import net.minidev.json.annotate.JsonIgnore;


@Entity
@Table(name="Hobby")
public class Hobby implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1442665206056041894L;

	
	
	@ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            },
            mappedBy = "hobbys")
	@JsonIgnore
    private Set<Person> persons = new HashSet<>();
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="title")
	private String title;
	
	protected Hobby() {
	}
 
	public Hobby(String title) {
		this.title = title;
		
	}

	@Override
	public String toString() {
		return "Hobby [persons=" + persons + ", id=" + id + ", title=" + title + "]";
	}

	public Set<Person> getPersons() {
		return persons;
	}

	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
