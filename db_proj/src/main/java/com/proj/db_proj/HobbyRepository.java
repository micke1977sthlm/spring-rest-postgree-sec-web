package com.proj.db_proj;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface HobbyRepository extends JpaRepository<Hobby, Long> {
	public List<Hobby> findByPersons_Id(Long id); 
	public List<Hobby> findByPersons_IdIn(Set<Long>ids); 
	
	@Query("select h from Hobby h")
    List<Hobby> findByHobbyHobbyId();
	
}
