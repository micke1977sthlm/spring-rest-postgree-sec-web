package com.proj.db_proj;

import java.util.List;
import java.util.Set;

public interface HobbyService {

	public Iterable<Hobby>findAll();
	public Hobby findOne(long id);
	public void save(Iterable<Hobby> hobby);
	public List<Hobby>findByPerson_Id(Long id);
	public List<Hobby>findByPerson_IdIn(Set<Long>ids);
	public List<Hobby>findByHobbyHobbyId();
}
