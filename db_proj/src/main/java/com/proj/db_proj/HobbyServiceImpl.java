package com.proj.db_proj;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class HobbyServiceImpl implements HobbyService {
	@Autowired
	HobbyRepository hobbyRepository;


	@Override
	public Iterable<Hobby> findAll() {
		
		return hobbyRepository.findAll();
	}
	@Override
	public Hobby findOne(long id) {
		return hobbyRepository.findOne(id);
	}

	@Override
	public void save(Iterable<Hobby> hobby) {
		hobbyRepository.save(hobby);
		
	}
	@Override
	public List<Hobby> findByPerson_Id(Long id) {
		
		return hobbyRepository.findByPersons_Id(id);
	}
	@Override
	public List<Hobby> findByPerson_IdIn(Set<Long> id) {
		
		return hobbyRepository.findByPersons_IdIn(id);
	}
	@Override
	public List<Hobby> findByHobbyHobbyId() {
		// TODO Auto-generated method stub
		return hobbyRepository.findByHobbyHobbyId();
	}
	
	
	
	


	
}
