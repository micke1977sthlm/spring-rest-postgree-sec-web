package com.proj.db_proj;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PersonRepository  extends JpaRepository<Person, Long>{

	List<Person>findByLastName(String lastName);
	List<Person> findByHobbys_Id(Long id); 
	List<Person> findByHobbys_IdIn(Set<Long> ids); 
	
	
}
