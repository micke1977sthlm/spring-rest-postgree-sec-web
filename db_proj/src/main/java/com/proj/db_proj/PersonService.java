package com.proj.db_proj;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.data.repository.query.Param;



public interface PersonService {
	public Iterable<Person>findAll();
	public Person findOne(long id);
	public List<Person>findByLastName(String lastName);
	public List<Person>findByHobby_Id(long id);
	public List<Person>findByHobby_IdIn(Set<Long> ids);
	public void save(Iterable<Person> persons);
	
}
