package com.proj.db_proj;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
public class PersonServiceImpl implements PersonService{
	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public Iterable<Person> findAll() {
		
		return personRepository.findAll();
	}

	@Override
	public Person findOne(long id) {
		return personRepository.findOne(id);
	}

	@Override
	public List<Person> findByLastName(String lastName) {
		return personRepository.findByLastName(lastName);
	}

	@Override
	public void save(Iterable<Person> persons) {
		personRepository.save(persons);
		
	}

	

	
	@Override
	public List<Person> findByHobby_Id(long id) {
		return findByHobby_Id(id);
	}

	@Override
	public List<Person> findByHobby_IdIn(Set<Long> ids) {
		return findByHobby_IdIn(ids);
	}

	
	
	

	

}
