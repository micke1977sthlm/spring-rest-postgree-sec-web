package com.proj.db_proj;


public class Response {
	private String status;
	private String jsonData;
 
	public Response() {
 
	}
 
	public Response(String status, String jsonData) {
		this.status = status;
		this.jsonData = jsonData;
		
	}
 
	public String getStatus() {
		return status;
	}
 
	public String getJsonData() {
		return jsonData;
	}

	public void setData(String jsonData) {
		this.jsonData = jsonData;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
}