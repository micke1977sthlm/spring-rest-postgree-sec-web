package com.proj.db_proj;



import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebRestController {

	
	@Autowired 
	PersonService personService;
	
	@Autowired 
	HobbyService hobbyService;
	
	
	//experimenting on different mappings/responses etc
	
	@RequestMapping("/save")
	public String save()
	{
		personService.save(Arrays.asList(new Person("Adam", "Johnson"),new Person("Nils", "Svensson"),new Person("Anna", "Ohlsson"),new Person("Camilla", "Nilsson"),new Person("Arne", "Karlsson")));
		return "Done";
		
	}
	
	@RequestMapping("/saveHobby")
	public String saveHobby()
	{
		hobbyService.save(Arrays.asList(new Hobby("Fotboll"),new Hobby("frimärken"),new Hobby("Bio")));
		return "Done";
		
	}
	
	@RequestMapping("/savePersonHobby")
	public String savePersonHobby()
	{
		Hobby hobbyFotboll=new Hobby("Fotboll");
		Hobby hobbyFrimarken=new Hobby("Frimärken");
		Hobby hobbyFilm=new Hobby("Film");
		Hobby hobbyGolf=new Hobby("Golf");
		
		Person adamJohnson=new Person("Adam", "Johnson");
		Person nilsSvensson=new Person("Nils", "Svensson");
		Person annaPersson=new Person("Anna", "Persson");
		
		adamJohnson.getHobbys().add(hobbyFotboll);
		hobbyFotboll.getPersons().add(adamJohnson);
		adamJohnson.getHobbys().add(hobbyFilm);
		hobbyFilm.getPersons().add(adamJohnson);
		
		nilsSvensson.getHobbys().add(hobbyGolf);
		hobbyGolf.getPersons().add(nilsSvensson);
		
		annaPersson.getHobbys().add(hobbyFilm);
		hobbyFilm.getPersons().add(annaPersson);
		annaPersson.getHobbys().add(hobbyFrimarken);
		hobbyFrimarken.getPersons().add(annaPersson);
		annaPersson.getHobbys().add(hobbyGolf);
		hobbyGolf.getPersons().add(annaPersson);
		
		
		
		
		
		
		personService.save(Arrays.asList(adamJohnson,nilsSvensson,annaPersson));
		return "Done";
		
	}
	
	
	
	@RequestMapping("/findall")
	public String findAll(){
		String result = "";
		
		for(Person cust : personService.findAll()){
			result += cust.toString() + "<br>";
		}
		return result;
	}
	
	@RequestMapping("/findallresponse")
	public String findAllResponse(){	
		ArrayList firstNameStr=new ArrayList<String>();
		ArrayList lastNameStr=new ArrayList<String>();
		ArrayList hobbyStr=new ArrayList<String>();
		ArrayList idStr=new ArrayList<String>();
		

		
		boolean startLoop=true;
		String jsonPersonBeggining="{\"persons\":[";
		String jsonPersonEnd="]}";
		String jsonPerson="";
		for(Person person : personService.findAll()){
			if(startLoop==true)
			{
				startLoop=false;
			}
			else
			{
				jsonPerson+=",";
			}
			jsonPerson+="{";
			
			jsonPerson+="\"firstName\":\""+person.getFirstName()+"\",";
			jsonPerson+="\"lastName\":\""+person.getLastName()+"\",";
			jsonPerson+="\"hobby\":[";
					boolean startLoopHobby=true;
					for(Hobby h:person.getHobbys())
					{
						if(startLoopHobby==true)
						{
							startLoopHobby=false;
						}
						else
						{
							jsonPerson+=",";
						}
						jsonPerson+="{";
						jsonPerson+="\"title\":\""+h.getTitle()+"\",";
						jsonPerson+="\"id\":\""+h.getId()+"\"";
						jsonPerson+="}";
							
					}
					
					
			jsonPerson+="],";
			jsonPerson+="\"id\":\""+person.getId()+"\"";
			
			
			
			jsonPerson+="}";
			
		}
		
		jsonPerson=jsonPersonBeggining+jsonPerson+jsonPersonEnd;
		
		
		return new Response("Done",jsonPerson).getJsonData();
	}
	
	@RequestMapping("/findallResponseEntity")
	public ResponseEntity<?> findAllRespEntity() {
		Iterable<Person> personServiceIterable=personService.findAll();
		
		List<String> personListToString=new ArrayList<String>();
		
		personServiceIterable.forEach(person->{
			personListToString.add(person.toString());
		});
		
		
		
		URI location=null;
		try {
			location=new URI("http://www.web.com/user/id/");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		return ResponseEntity.created(location).header("MyResponseHeader", "MyValue").body(personListToString);
	}
	
	
	
	@GetMapping("/findallhobby")
	public String findAllHobby(){
		ArrayList hobbyName=new ArrayList<String>();
		ArrayList idStr=new ArrayList<String>();
		
		boolean startLoop=true;
		String jsonHobbyBeggining="{\"hobby\":[";
		String jsonHobbyEnd="]}";
		String jsonHobby="";
		for(Hobby h : hobbyService.findAll()){
			if(startLoop==true)
			{
				startLoop=false;
			}
			else
			{
				jsonHobby+=",";
			}
			jsonHobby+="{";
			
			jsonHobby+="\"title\":\""+h.getTitle()+"\",";
			jsonHobby+="\"id\":\""+h.getId()+"\"";
						
			
			
			jsonHobby+="}";
			
		}
		
		jsonHobby=jsonHobbyBeggining+jsonHobby+jsonHobbyEnd;
		
		
		return new Response("Done",jsonHobby).getJsonData();
	}
	
	@GetMapping("/hobby/{id}")
	public String findOneHobby(@PathVariable long id) {
		String result = "";
		result = hobbyService.findOne(id).toString();
		return result;
	}
	
	@RequestMapping("/findbyid")
	public String findById(@RequestParam("id") long id){
		String result = "";
		result = personService.findOne(id).toString();
		return result;
	}
	
	@RequestMapping("/findbypersonid/{id}")
	public String findByPersonId(@PathVariable long id){
		String result = "";
		
		result = hobbyService.findByPerson_Id(id).toString();
		return result;
	}
	
	@RequestMapping("/findbypersonidIn/{id}")
	public String findByPersonIdIn(@PathVariable long id){
		String result = "";
		Set <Long>ids=new HashSet<Long>();
		ids.add(new Long(87));
		ids.add(new Long(90));
		//result = hobbyService.findByPerson_IdIn(ids).toString();
		for(Hobby cust : hobbyService.findByPerson_IdIn(ids)){
			result += cust.toString() + "<br>______________<br>";
		}
		return result;
	}
	
	
	@RequestMapping("/findbyhobbyid/{id}")
	public String findByHobbyId(@PathVariable long id){
		String result = "";
		result =personService.findByHobby_Id(new Long(88)).toString();
		return result;
	}
	
	@RequestMapping("/findbylastname")
	public String fetchDataByLastName(@RequestParam("lastname") String lastName){
		String result = "";

		for(Person cust: personService.findByLastName(lastName)){
			result += cust.toString() + "<br>"; 
			System.out.println(result);
		}
		
		return result;
	}
	
	@RequestMapping("/findbyQuery")
	public String fetchDataByQuery(){
		String result = "";

		for(Hobby cust: hobbyService.findByHobbyHobbyId()){
			result += cust.toString() + "<br>"; 
			System.out.println(result);
		}
		
		return result;
	}
	
	
	
	
	
	
}
