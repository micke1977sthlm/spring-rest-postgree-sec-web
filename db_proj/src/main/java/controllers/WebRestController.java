package controllers;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import entities.Person;
import repositories.PersonRepository;

@RestController
public class WebRestController {

	@Autowired
	PersonRepository personRepository;
	
	@RequestMapping("/save")
	public String save()
	{
		personRepository.save(Arrays.asList(new Person("Adam", "Johnson",1),new Person("Nils", "Svensson",1),new Person("Anna", "Ohlsson",1),new Person("Camilla", "Nilsson",1),new Person("Arne", "Karlsson",1)));
		return "Done";
		
	}
	
	@RequestMapping("/findall")
	public String findAll(){
		String result = "";
		
		for(Person cust : personRepository.findAll()){
			result += cust.toString() + "<br>";
		}
		
		return result;
	}
	
	@RequestMapping("/findbyid")
	public String findById(@RequestParam("id") long id){
		String result = "";
		result = personRepository.findOne(id).toString();
		return result;
	}
	
	@RequestMapping("/findbylastname")
	public String fetchDataByLastName(@RequestParam("lastname") String lastName){
		String result = "";
		
		for(Person cust: personRepository.findByLastName(lastName)){
			result += cust.toString() + "<br>"; 
		}
		
		return result;
	}
}
