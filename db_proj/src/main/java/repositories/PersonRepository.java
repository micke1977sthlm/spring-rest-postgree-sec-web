package repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import entities.Person;

public interface PersonRepository  extends CrudRepository<Person, Long>{

	List<Person>findByLastName(String lastName);
}
