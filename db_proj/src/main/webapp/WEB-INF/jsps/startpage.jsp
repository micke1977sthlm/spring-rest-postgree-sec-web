<!DOCTYPE html>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<body>
<div ng-app="myApp">
	<div  ng-controller="personsAndHobbiesCtrl">
		<button ng-click="getPersonsAndHobbies()">Get Persons & Hobbies</button>
		<div ng-show="showPersonsAndHobbies">
			<table>
			  <tr ng-repeat="x in personandhobby">
			    <td>{{ x.firstName }} {{ x.lastName }} {{ x.id }} 
			    	<ul>
				    	<li ng-repeat="y in x.hobby">
				    		{{y.title}}
				    	</li>
			    	</ul>
			    		
			     </td>
			  </tr>
			</table>	
		</div>
	</div>
	<div  ng-controller="hobbiesCtrl">
		<button ng-click="getHobbies()">Get Hobbies</button>
		<div ng-show="showHobbies">
		
			<table>
				<tr ng-repeat="x in hobby">
				    <td>
				    	<p><input type="checkbox" ng-click="getHobbiesSel($index,x.id)"/>Name:{{ x.title }} Id: {{ x.id }} </p>
				    	
				    </td>
				</tr>
			</table>
			<button ng-click="searchHobbiesById()">Search!</button>
			{{textStr}}
		</div>
	</div>
</div>
<script>
var app = angular.module('myApp', []);
app.controller('personsAndHobbiesCtrl', function($scope, $http,$location) {
		$scope.showPersonsAndHobbies=false;
		
		$scope.getPersonsAndHobbies = function() {
		    $http.get($location.absUrl() + "/findallresponse")
		    .then(function (response) {
		    		$scope.personandhobby = response.data.persons;
		    		$scope.showPersonsAndHobbies=true;
		    	});
		}
});

app.controller('hobbiesCtrl', function($scope, $http,$location) {
	$scope.showHobbies=false;
	$scope.hobbiesSel=[];
	$scope.selectedHobbiesId=[];
	$scope.hobbySize=-1;
	$scope.textStr="hej";
	
	$scope.getHobbies = function() {
	    $http.get($location.absUrl() + "/findallhobby")
	    .then(function (response) {
	    		$scope.hobby = response.data.hobby;
	    		$scope.hobbySize=$scope.hobby.length;
	    		for(var i=0;i<$scope.hobbySize;i++)
	    		{
	    			$scope.hobbiesSel[i]=false;
	    			$scope.selectedHobbiesId[i]=-1;
	    		}
	    		$scope.showHobbies=true;
	    	});
	}
	
	$scope.getHobbiesSel = function(index,id) {
		
		if($scope.hobbiesSel[index])
		{
			$scope.hobbiesSel[index]=false;
			$scope.selectedHobbiesId[index]=-1;
		}
		else
		{
			$scope.hobbiesSel[index]=true;
			$scope.selectedHobbiesId[index]=id;
		}
		
	}
	
	$scope.searchHobbiesById = function()
	{
		textStr="�����";
	}
	
});
</script>

</body>
</html>
